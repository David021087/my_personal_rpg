package com.example.david.myandroidrpg.gear;

/**
 * Created by David on 06/05/2017.
 */

public interface Weapon {

    String weaponName();
    String weaponSpecialEffect();
    String weaponUsageSentence();
    int damageEnforcement();
    int accuracyBonus();
}
