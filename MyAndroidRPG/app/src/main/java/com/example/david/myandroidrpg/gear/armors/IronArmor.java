package com.example.david.myandroidrpg.gear.armors;

import com.example.david.myandroidrpg.gear.Armor;

/**
 * Created by David on 06/05/2017.
 */

public class IronArmor implements Armor {

    private String name, specialEffect;
    private int damageResistance, defenseBonus;

    public IronArmor(){
        this.name = "Iron Armor.";
        this.specialEffect = "None.";
        this.damageResistance = 2;
        this.defenseBonus = 1;

    }

    @Override
    public String armorName() {
        return name;
    }

    @Override
    public String armorSpecialEffect() {
        return specialEffect;
    }

    @Override
    public int damageResistance() {
        return damageResistance;
    }

    @Override
    public int defenseBonus() {
        return defenseBonus;
    }
}
