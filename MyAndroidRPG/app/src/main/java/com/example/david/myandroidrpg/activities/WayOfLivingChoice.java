package com.example.david.myandroidrpg.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.david.myandroidrpg.R;

public class WayOfLivingChoice extends AppCompatActivity {

    private static String lifeChoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_of_living_choice);

        Button warrior = (Button)findViewById(R.id. button_warrior);
        Button mage = (Button)findViewById(R.id. button_mage);
        Button rogue = (Button)findViewById(R.id.button_rogue);

        warrior.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Intent warrior = new Intent(WayOfLivingChoice.this, LifeDescription.class);
                lifeChoice="warrior";
                startActivity(warrior);


            }
        });

        mage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mage = new Intent(WayOfLivingChoice.this, LifeDescription.class);
                lifeChoice="mage";
                startActivity(mage);

            }
        });

        rogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent rogue = new Intent (WayOfLivingChoice.this, LifeDescription.class);
                lifeChoice="rogue";
                startActivity(rogue);
            }
        });
    }

    public static String getLifeChoice() {
        return lifeChoice;
    }

}
