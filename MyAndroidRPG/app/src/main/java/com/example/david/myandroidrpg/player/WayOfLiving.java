package com.example.david.myandroidrpg.player;

/**
 * Created by David on 06/05/2017.
 */

public interface WayOfLiving {

    String nameWayOfLife();
    String introduction();
    int  healthPoints();
    int staminaPoints();
    int  forceRate();
    int  constitutionRate();
    int  dexterityRate();
    int  intelligenceRate();
    int  wisenessRate();
    int  charismaRate();
}
