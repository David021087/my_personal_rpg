package com.example.david.myandroidrpg.player.character_ways_of_living;


import com.example.david.myandroidrpg.player.WayOfLiving;

/**
 * Created by David on 06/05/2017.
 */

public class Mage implements WayOfLiving {

    private int healthPoints, staminaPoints, forceRate, constitutionRate, dexterityRate, intelligenceRate, wisenessRate, charismaRate;
    private String name, introduction;
    public Mage(){
        //100 points
        this.name= "mage";
        this.introduction = "Since your fifth year of life, you have ever craved the secrets of " +
                "magic and went therefore to the Academy of Wizzardy.";
        this.healthPoints = 0;
        this.staminaPoints=0;
        this.forceRate = 10;
        this.constitutionRate = 8;
        this.dexterityRate = 15;
        this.intelligenceRate = 30;
        this.wisenessRate = 25;
        this.charismaRate = 12;
    }

    @Override
    public String nameWayOfLife() {
        return this.name;
    }

    @Override
    public String introduction() {
        return introduction;
    }

    @Override
    public int healthPoints() {
        return this.healthPoints;
    }

    @Override
    public int staminaPoints() {
        return staminaPoints;
    }

    @Override
    public int forceRate() {
        return this.forceRate;
    }

    @Override
    public int constitutionRate() {
        return this.constitutionRate;
    }

    @Override
    public int dexterityRate() {
        return this.dexterityRate;
    }

    @Override
    public int intelligenceRate() {
        return this.intelligenceRate;
    }

    @Override
    public int wisenessRate() {
        return this.wisenessRate;
    }

    @Override
    public int charismaRate() {
        return this.charismaRate;
    }
}
