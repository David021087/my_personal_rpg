package com.example.david.myandroidrpg.gear.weapons;

import com.example.david.myandroidrpg.gear.Weapon;

/**
 * Created by David on 06/05/2017.
 */

public class IronSword implements Weapon {

    private String name, usageSentence, specialEffect;
    private int damageEnforcement, accuracyBonus;

    public IronSword(){
        this.name = "Iron Sword";
        this.specialEffect = "None.";
        this.usageSentence = "The blade slices through the air towards your opponent.";
        this.damageEnforcement=3;
        this.accuracyBonus=2;

    }


    @Override
    public String weaponName() {
        return name;
    }

    @Override
    public String weaponSpecialEffect() {
        return specialEffect;
    }

    @Override
    public String weaponUsageSentence() {
        return usageSentence;
    }

    @Override
    public int damageEnforcement() {
        return damageEnforcement;
    }

    @Override
    public int accuracyBonus() {
        return accuracyBonus;
    }
}
