package com.example.david.myandroidrpg.gear.armors;

import com.example.david.myandroidrpg.gear.Armor;

/**
 * Created by David on 06/05/2017.
 */

public class MageCloak implements Armor {

    private String name, specialEffect;
    private int damageResistance, defenseBonus;

    public MageCloak(){
        this.name = "Mage Cloak.";
        this.specialEffect = "None.";
        this.damageResistance = 2;
        this.defenseBonus = 0;

    }

    @Override
    public String armorName() {
        return name;
    }

    @Override
    public String armorSpecialEffect() {
        return specialEffect;
    }

    @Override
    public int damageResistance() {
        return damageResistance;
    }

    @Override
    public int defenseBonus() {
        return defenseBonus;
    }
}
