package com.example.david.myandroidrpg.gear;

/**
 * Created by David on 06/05/2017.
 */

public interface Armor {

    String armorName();
    String armorSpecialEffect();
    int damageResistance();
    int defenseBonus();
}
