package com.example.david.myandroidrpg.activities;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.david.myandroidrpg.R;
import com.example.david.myandroidrpg.player.Player;


public class RaceDescription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_race_description);

        TextView raceDescription = (TextView)findViewById(R.id.textview_race_description);

        if (RaceChoice.getChoiceRace().equals("human")){
            raceDescription.setText(R.string.humanrace);
        }else if (RaceChoice.getChoiceRace().equals("elf")){
            raceDescription.setText(R.string.elfrace);
        }else if (RaceChoice.getChoiceRace().equals("dwarf")){
            raceDescription.setText(R.string.dwarfrace);
        }

        Button back = (Button)findViewById(R.id.backbutton);
        Button confirm = (Button)findViewById(R.id.confirmbutton);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.setRace(RaceChoice.getChoiceRace());
                Intent confirm = new Intent(RaceDescription.this, WayOfLivingChoice.class);
                startActivity(confirm);
            }
        });
    }
}
