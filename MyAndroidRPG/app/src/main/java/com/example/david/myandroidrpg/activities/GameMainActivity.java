package com.example.david.myandroidrpg.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.david.myandroidrpg.R;

public class GameMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_main);
    }
}
