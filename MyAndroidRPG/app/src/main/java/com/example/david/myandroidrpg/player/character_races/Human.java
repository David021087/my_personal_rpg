package com.example.david.myandroidrpg.player.character_races;


import com.example.david.myandroidrpg.player.Race;

/**
 * Created by David on 06/05/2017.
 */

public class Human implements Race {


    private int healthPoints, staminaPoints, forceRate, constitutionRate, dexterityRate, intelligenceRate, wisenessRate, charismaRate;
    private String name, intro;
    public Human() {

        // 220 points
        this.name = "human";
        this.intro = "You are a young human born in the village of Ironcast.";
        this.healthPoints = 10;
        this.staminaPoints=5;
        this.forceRate = 50;
        this.constitutionRate = 52;
        this.dexterityRate = 30;
        this.intelligenceRate = 25;
        this.wisenessRate = 20;
        this.charismaRate = 43;
    }

    @Override
    public String nameRace() {
        return this.name;
    }

    @Override
    public String introduction() {
        return this.intro;
    }

    @Override
    public int healthPoints() {
        return this.healthPoints;
    }

    @Override
    public int staminaPoints() {
        return this.staminaPoints;
    }

    @Override
    public int forceRate() {
        return this.forceRate;
    }

    @Override
    public int constitutionRate() {
        return this.constitutionRate;
    }

    @Override
    public int dexterityRate() {
        return this.dexterityRate;
    }

    @Override
    public int intelligenceRate() {
        return this.intelligenceRate;
    }

    @Override
    public int wisenessRate() {
        return this.wisenessRate;
    }

    @Override
    public int charismaRate() {
        return this.charismaRate;
    }

}
