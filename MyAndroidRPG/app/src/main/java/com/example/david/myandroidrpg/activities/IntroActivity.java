package com.example.david.myandroidrpg.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.example.david.myandroidrpg.R;
import com.example.david.myandroidrpg.player.Player;


public class IntroActivity extends AppCompatActivity {
    private TextView intro;
    private String firstIntroText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        intro = (TextView) findViewById(R.id.textview_intro);
        intro.setText(setFirstIntroText());


        Button nextButton = (Button) findViewById(R.id.button_next);


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IntroActivity.this, IntroActivity2.class);
                startActivity(intent);
            }
        });

    }

    private String setFirstIntroText() {
        if (Player.getRace().equals("human")){
            if(Player.getWayofliving().equals("warrior")){
                firstIntroText = getString(R.string.humanintro)+"\n"+getString(R.string.warriorintro);

            }else if (Player.getWayofliving().equals("mage")){
                firstIntroText = getString(R.string.humanintro)+"\n"+getString(R.string.mageintro);

            }else if (Player.getWayofliving().equals("rogue")){
                firstIntroText = getString(R.string.humanintro)+"\n"+getString(R.string.rogueintro);
            }
        }else if (Player.getRace().equals("elf")){
            if(Player.getWayofliving().equals("warrior")){
                firstIntroText = getString(R.string.elfintro)+"\n"+getString(R.string.warriorintro);
            }else if (Player.getWayofliving().equals("mage")){
                firstIntroText = getString(R.string.elfintro)+"\n"+getString(R.string.mageintro);

            }else if (Player.getWayofliving().equals("rogue")){
                firstIntroText = getString(R.string.elfintro)+"\n"+getString(R.string.rogueintro);
            }
        }else if (Player.getRace().equals("dwarf")){
            if(Player.getWayofliving().equals("warrior")){
                firstIntroText = getString(R.string.dwarfintro)+"\n"+getString(R.string.warriorintro);
            }else if (Player.getWayofliving().equals("mage")){
                firstIntroText = getString(R.string.dwarfintro)+"\n"+getString(R.string.mageintro);
            }else if (Player.getWayofliving().equals("rogue")){
                firstIntroText = getString(R.string.elfintro)+"\n"+getString(R.string.rogueintro);
            }
        }
        return firstIntroText;
    }
}
