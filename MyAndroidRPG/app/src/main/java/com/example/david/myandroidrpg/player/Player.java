package com.example.david.myandroidrpg.player;

import com.example.david.myandroidrpg.R;
import com.example.david.myandroidrpg.gear.Armor;
import com.example.david.myandroidrpg.gear.Weapon;
import com.example.david.myandroidrpg.gear.armors.IronArmor;
import com.example.david.myandroidrpg.gear.armors.MageCloak;
import com.example.david.myandroidrpg.gear.weapons.CarvedWoodenRod;
import com.example.david.myandroidrpg.gear.weapons.IronSword;
import com.example.david.myandroidrpg.player.character_races.Human;
import com.example.david.myandroidrpg.player.character_ways_of_living.Mage;
import com.example.david.myandroidrpg.player.character_ways_of_living.Warrior;

/**
 * Created by David on 06/05/2017.
 */

public class Player {

    private static int maxHP, remainHP, maxStamina, remainStamina, force, constitution, dexterity, intelligence, wiseness, charisma;
    private static int attack, defense;
    private static String race, wayofliving;
    private static String currentWeapon, currentArmor;
    private static int damageBonus, attackBonus;
    private static int defenseBonus;
    private static String raceintroduction, lifeintroduction, playerIntroduction;
    private static String raceName, wayOfLifeName;


    public Player(){





        Weapon ironSword = new IronSword();
        Weapon carvedWoodenRod = new CarvedWoodenRod();
        Armor ironArmor = new IronArmor();
        Armor mageCloak = new MageCloak();


        boolean isHuman = race.equals("human");
        boolean isElf = race.equals("elf");
        boolean isDwarf = race.equals("dwarf");
        boolean isWarrior = wayofliving.equals("warrior");
        boolean isMage = wayofliving.equals("mage");
        if(isHuman) {
            Race human = new Human();
            raceName = human.nameRace();
            raceintroduction=human.introduction();
            if (isWarrior) {
                WayOfLiving warrior = new Warrior();
                lifeintroduction=warrior.introduction();
                currentWeapon = ironSword.weaponName();
                damageBonus = ironSword.damageEnforcement();
                attackBonus = ironSword.accuracyBonus();
                currentArmor = ironArmor.armorName();
                defenseBonus = ironArmor.defenseBonus();
                wayOfLifeName = warrior.nameWayOfLife();
                maxHP = human.healthPoints() + warrior.healthPoints();
                maxStamina =human.staminaPoints()+warrior.staminaPoints();
                force = human.forceRate() + warrior.forceRate();
                constitution = human.constitutionRate() + warrior.constitutionRate();
                dexterity = human.dexterityRate() + warrior.dexterityRate();
                intelligence = human.intelligenceRate() + warrior.intelligenceRate();
                wiseness = human.wisenessRate() + warrior.wisenessRate();
                charisma = human.charismaRate() + warrior.charismaRate();
                remainHP= maxHP;
                remainStamina= maxStamina;
            } else if (isMage) {
                WayOfLiving mage = new Mage();
                lifeintroduction = mage.introduction();
                currentWeapon = carvedWoodenRod.weaponName();
                damageBonus = carvedWoodenRod.damageEnforcement();
                attackBonus = carvedWoodenRod.damageEnforcement();
                currentArmor = mageCloak.armorName();
                defenseBonus = mageCloak.defenseBonus();
                wayOfLifeName = mage.nameWayOfLife();
                maxHP = human.healthPoints() + mage.healthPoints();
                maxStamina =human.staminaPoints()+mage.staminaPoints();
                force = human.forceRate() + mage.forceRate();
                constitution = human.constitutionRate() + mage.constitutionRate();
                dexterity = human.dexterityRate() + mage.dexterityRate();
                intelligence = human.intelligenceRate() + mage.intelligenceRate();
                wiseness = human.wisenessRate() + mage.wisenessRate();
                charisma = human.charismaRate() + mage.charismaRate();
                remainHP= maxHP;
                remainStamina= maxStamina;

            }
        }
        attack = force/10+damageBonus;
        defense = constitution/10+defenseBonus;






    }



    public static int getMaxHP() {
        return maxHP;
    }

    public static int getForce() {
        return force;
    }

    public static int getConstitution() {
        return constitution;
    }

    public static int getDexterity() {
        return dexterity;
    }

    public static int getIntelligence() {
        return intelligence;
    }

    public static int getWiseness() {
        return wiseness;
    }

    public static int getCharisma() {
        return charisma;
    }

    public static int getRemainHP() {
        return remainHP;
    }

    public static int getMaxStamina() {
        return maxStamina;
    }

    public static int getRemainStamina() {
        return remainStamina;
    }

    public static int getAttack() {
        return attack;
    }

    public static int getDefense() {
        return defense;
    }

    public static String getRace() {
        return race;
    }

    public static String getWayofliving() {
        return wayofliving;
    }

    public static void setRace(String race) {
        Player.race = race;
    }

    public static void setWayofliving(String wayofliving) {
        Player.wayofliving = wayofliving;
    }
}
