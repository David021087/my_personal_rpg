package com.example.david.myandroidrpg.gear.weapons;

import com.example.david.myandroidrpg.gear.Weapon;

/**
 * Created by David on 06/05/2017.
 */

public class CarvedWoodenRod implements Weapon {

    private String name, usageSentence, specialEffect;
    private int damageEnforcement, accuracyBonus;

    public CarvedWoodenRod(){
        this.name = "Carved Wooden Rod";
        this.specialEffect = "Shoots small fireballs.";
        this.usageSentence = "You shoot a small fireball towards your opponent.";
        this.damageEnforcement=0;
        this.accuracyBonus=1;

    }


    @Override
    public String weaponName() {
        return name;
    }

    @Override
    public String weaponSpecialEffect() {
        return specialEffect;
    }

    @Override
    public String weaponUsageSentence() {
        return usageSentence;
    }

    @Override
    public int damageEnforcement() {
        return damageEnforcement;
    }

    @Override
    public int accuracyBonus() {
        return accuracyBonus;
    }
}
