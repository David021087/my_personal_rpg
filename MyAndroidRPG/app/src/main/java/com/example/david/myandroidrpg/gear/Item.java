package com.example.david.myandroidrpg.gear;

/**
 * Created by David on 06/05/2017.
 */

public interface Item {

    String itemName();
    String itemEffect();
    String intemUsageSentence();
    int itemWeight();
}
