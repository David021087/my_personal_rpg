package com.example.david.myandroidrpg.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.david.myandroidrpg.R;
import com.example.david.myandroidrpg.player.Player;

public class LifeDescription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_description);

        TextView lifeDescription = (TextView)findViewById(R.id.textview_life_description);

        if (WayOfLivingChoice.getLifeChoice().equals("warrior")){
            lifeDescription.setText(R.string.warriorlife);
        }else if (WayOfLivingChoice.getLifeChoice().equals("mage")){
            lifeDescription.setText(R.string.magelife);
        }else if (WayOfLivingChoice.getLifeChoice().equals("rogue")){
            lifeDescription.setText(R.string.roguelife);

        }

        Button back = (Button)findViewById(R.id.backbutton);
        Button confirm = (Button)findViewById(R.id.confirmbutton);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player.setWayofliving(WayOfLivingChoice.getLifeChoice());
                Intent confirm = new Intent(LifeDescription.this, IntroActivity.class);
                startActivity(confirm);
            }
        });
    }
}
