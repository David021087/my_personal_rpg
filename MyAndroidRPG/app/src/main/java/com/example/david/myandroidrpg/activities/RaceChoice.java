package com.example.david.myandroidrpg.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.david.myandroidrpg.R;

public class RaceChoice extends AppCompatActivity {
private static String choiceRace;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_choice);

        Button human = (Button)findViewById(R.id. button_human);
        Button elf = (Button)findViewById(R.id. button_elf);
        Button dwarf = (Button)findViewById(R.id.button_dwarf);

        human.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Intent human = new Intent(RaceChoice.this, RaceDescription.class);
                choiceRace="human";
                startActivity(human);


            }
        });

        elf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent elf = new Intent(RaceChoice.this, RaceDescription.class);
                choiceRace="elf";
                startActivity(elf);

            }
        });

        dwarf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dwarf = new Intent (RaceChoice.this, RaceDescription.class);
                choiceRace="dwarf";
                startActivity(dwarf);
            }
        });
    }

    public static String getChoiceRace() {
        return choiceRace;
    }
}
