package com.example.david.myandroidrpg.player.character_ways_of_living;


import com.example.david.myandroidrpg.player.WayOfLiving;

/**
 * Created by David on 06/05/2017.
 */

public class Warrior implements WayOfLiving {

    private int healthPoints, staminaPoints, forceRate, constitutionRate, dexterityRate, intelligenceRate, wisenessRate, charismaRate;
    private String name, introduction;
    public Warrior() {
        //100 points
        this.name = "human";
        this.introduction= "You always wanted to be a strong and robust warrior, respected by\n" +
                "        his friends and feared by his ennemies. Hence you studied the ways armed combat and diplomacy.\n" +
                "                After years of training, here comes your final test. There is an old abandoned castle an hour\n" +
                "        away from your home village, which is crawling with despicable creatures.\\n\n" +
                "        Your task: go there, clean the place and come back with a proof of your feat.\n" +
                "        \\\"Bring us back the head of their chief, and the way to become a warrior you will achieve.\\\"\n" +
                "                , told you Terebor the swordmaster.";
        this.healthPoints = 10;
        this.staminaPoints=5;
        this.forceRate = 20;
        this.constitutionRate = 30;
        this.dexterityRate = 23;
        this.intelligenceRate = 15;
        this.wisenessRate = 5;
        this.charismaRate = 7;
    }

    @Override
    public String nameWayOfLife() {
        return this.name;
    }

    @Override
    public String introduction() {
        return introduction;
    }

    @Override
    public int healthPoints() {
        return this.healthPoints;
    }

    @Override
    public int staminaPoints() {
        return this.staminaPoints;
    }

    @Override
    public int forceRate() {
        return this.forceRate;
    }

    @Override
    public int constitutionRate() {
        return this.constitutionRate;
    }

    @Override
    public int dexterityRate() {
        return this.dexterityRate;
    }

    @Override
    public int intelligenceRate() {
        return this.intelligenceRate;
    }

    @Override
    public int wisenessRate() {
        return this.wisenessRate;
    }

    @Override
    public int charismaRate() {
        return this.wisenessRate;
    }
}
